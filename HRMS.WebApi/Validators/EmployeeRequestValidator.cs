﻿namespace HRMS.WebApi.Validators
{
    using FluentValidation;
    using HRMS.WebApi.Models;

    public class EmployeeRequestValidator : AbstractValidator<Employee>
    {
        public EmployeeRequestValidator()
        {
            RuleFor(employee => employee.FirstName).NotEmpty().NotNull();
            RuleFor(employee => employee.LastName).NotEmpty().NotNull();
        }
    }
}
