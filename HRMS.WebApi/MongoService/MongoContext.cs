﻿namespace HRMS.WebApi.MongoService
{
    using System.Configuration;
    using MongoDB.Driver;

    public class MongoContext<T> where T : class 
    {
        public MongoContext()
        {
            var connectionString = new MongoConnectionStringBuilder(ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString);

            var mongoServer = MongoServer.Create(connectionString);
            var mongoDatabase = mongoServer.GetDatabase(connectionString.DatabaseName);
            Collection = mongoDatabase.GetCollection<T>(typeof(T).Name);
        }

        public MongoCollection<T> Collection { get; set; }
    }
}
