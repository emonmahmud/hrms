﻿namespace HRMS.WebApi.ErrorHandling
{
    using Newtonsoft.Json;

    public class Error
    {
        public string Message { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ErrorCode { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Details { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string CorrelationId { get; set; }
    }
}
