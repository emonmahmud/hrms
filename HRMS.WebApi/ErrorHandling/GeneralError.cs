﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace HRMS.WebApi.ErrorHandling
{
    public enum GeneralError
    {
        [ApiError(HttpStatusCode.BadRequest, "The request was malformed")]
        RequestMalformed = 1001,

        [ApiError(HttpStatusCode.BadRequest, "The given business unit id was malformed")]
        InvalidBusinessUnitId = 1002,

        [ApiError(HttpStatusCode.BadRequest, "The given review id was malformed")]
        InvalidReviewId = 1003,

        [ApiError(HttpStatusCode.BadRequest, "The given consumer id was malformed")]
        InvalidConsumerId = 1004,

        [ApiError(HttpStatusCode.NotFound, "Business Unit was not found")]
        BusinessUnitNotFound = 1005,

        [ApiError(HttpStatusCode.NotFound, "Consumer was not found")]
        ConsumerNotFound = 1006,

        [ApiError(HttpStatusCode.BadRequest, "The given locale was malformed")]
        InvalidLocale = 1007,

        [ApiError(HttpStatusCode.NotFound, "Locale was not found")]
        LocaleNotFound = 1008,

        [ApiError(HttpStatusCode.NotFound, "Review was not found")]
        ReviewNotFound = 1009,

        [ApiError(HttpStatusCode.NotFound, "Resource was not found")]
        ResourceNotFound = 1010,

        [ApiError(HttpStatusCode.NotFound, "Business User was not found")]
        BusinessLoginNotFound = 1011,

        [ApiError(HttpStatusCode.BadRequest, "The request did not validate")]
        ValidationError = 1012,

        [ApiError(HttpStatusCode.BadRequest, "Processing request would result in invalid state")]
        BusinessLogicError = 1013,
    }
}
