﻿namespace HRMS.WebApi.ErrorHandling
{
    using System;
    using System.Net;

    [AttributeUsage(AttributeTargets.Field)]
    public class ApiErrorAttribute : Attribute
    {
        public ApiErrorAttribute(HttpStatusCode code, string message)
        {
            this.Code = code;
            this.Message = message;
        }

        public HttpStatusCode Code { get; set; }

        public string Message { get; set; }
    }
}
