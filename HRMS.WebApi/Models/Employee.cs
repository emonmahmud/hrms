﻿namespace HRMS.WebApi.Models
{
    using MongoDB.Bson;

    public class Employee
    {
        public ObjectId Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
