﻿namespace HRMS.WebApi.Controllers
{
    using HRMS.WebApi.ErrorHandling;
    using HRMS.WebApi.Models;
    using HRMS.WebApi.MongoService;
    using HRMS.WebApi.Request.Employee;
    using HRMS.WebApi.Response.Employee;
    using HRMS.WebApi.Validators;
    using MongoDB.Bson;
    using MongoDB.Driver.Builders;
    using System.ComponentModel;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    public class EmployeeController : ApiController
    {
        private readonly MongoContext<Employee> MongoContext;

        public EmployeeController()
        {
            MongoContext = new MongoContext<Employee>();
        }

        public EmployeeListResponse GetAll()
        {
            var employeeList = MongoContext.Collection.FindAll().ToList();

            var employeeListResponse = new EmployeeListResponse()
            {
                EmployeeList = employeeList.Select(employee => new Employee()
                {
                    Id = employee.Id,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName
                }).ToList()
            };
            return employeeListResponse;
        }

        public Employee GetById([FromUri] EmployeeIdRequest employeeIdRequest)
        {
            var employeeId = ParseId(employeeIdRequest.Id, GeneralError.RequestMalformed);
            var employeeList = MongoContext.Collection.FindAll().ToList();
            var employee = employeeList.Find(e => e.Id == employeeId);

            return employee;
        }

        [Description("Add employee")]
        public HttpResponseMessage Post([FromBody] Employee employee)
        {
            var employeeRequestValidator = new EmployeeRequestValidator();

            var results = employeeRequestValidator.Validate(employee);
            if (results.IsValid)
            {
                var employees = MongoContext.Collection;

                employees.Insert(employee);

                return Request.CreateResponse(HttpStatusCode.OK);   
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, results.Errors.FirstOrDefault().ErrorMessage);
            }
        }

        [Description("Update employee")]
        public HttpResponseMessage Put([FromUri] EmployeeIdRequest employeeIdRequest, [FromBody] Employee anEmployee)
        {
            var employeeId = ParseId(employeeIdRequest.Id, GeneralError.RequestMalformed);
            var employeeRequestValidator = new EmployeeRequestValidator();
            var employee = new Employee()
            {
                FirstName = anEmployee.FirstName,
                LastName = anEmployee.LastName
            };

            var results = employeeRequestValidator.Validate(employee);
            if (results.IsValid)
            {
                var employees = MongoContext.Collection;
                var query = Query<Employee>.EQ(e => e.Id, employeeId);
                var update = Update<Employee>.Set(e => e.FirstName, anEmployee.FirstName).Set(e => e.LastName, anEmployee.LastName);

                employees.Update(query, update);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, results.Errors.FirstOrDefault().ErrorMessage);
            }
            
        }

        [Description("Update employee")]
        public HttpResponseMessage Delete([FromUri] EmployeeIdRequest employeeIdRequest)
        {
            var employeeId = ParseId(employeeIdRequest.Id, GeneralError.RequestMalformed);

            var employees = MongoContext.Collection;

            var query = Query<Employee>.EQ(e => e.Id, employeeId);

            employees.Remove(query);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        protected ObjectId ParseId<T>(string value, T parsingError) where T : struct
        {
            ObjectId id;
            if (!ObjectId.TryParse(value, out id))
            {
                ThrowError(parsingError);
            }

            return id;
        }

        public void ThrowError<T>(T value, string details = null) where T : struct
        {
            var member = typeof(T).GetMember(value.ToString())[0];
            var attribute = (ApiErrorAttribute)member.GetCustomAttributes(typeof(ApiErrorAttribute), false)[0];

            var error = new Error { Message = attribute.Message, ErrorCode = (int)(object)value, Details = details };
            
            throw new HttpResponseException(Request.CreateResponse(attribute.Code, error));
        }
    }
}
