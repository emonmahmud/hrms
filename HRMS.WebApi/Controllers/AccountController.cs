﻿namespace HRMS.WebApi.Controllers
{
    using HRMS.WebApi.MongoService;
    using HRMS.WebApi.Repository.User;
    using HRMS.WebApi.Request.User;
    using HRMS.WebApi.Response.User;
    using MongoDB.Bson;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    public class AccountController : ApiController
    {
        static readonly IUserRepository UserRepository = new UserRepository();

        public UserListResponse GetAll()
        {
            return UserRepository.GetAll();
        }
        
        public User GetById([FromUri] UserIdRequest userIdRequest)
        {
            var objId = ObjectId.Parse(userIdRequest.Id);

            var anUser = UserRepository.Get(objId);

            return anUser;
        }
        
        public HttpResponseMessage Post([FromBody] User anUser)
        {
            anUser = UserRepository.Add(anUser);

            return Request.CreateResponse(HttpStatusCode.OK, anUser);
        }

        public void Put([FromUri] UserIdRequest userIdRequest, [FromBody] User anUser)
        {
            var objId = ObjectId.Parse(userIdRequest.Id);

            if (!UserRepository.Update(objId, anUser))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public HttpResponseMessage Delete([FromUri] UserIdRequest userIdRequest)
        {
            var userId = ObjectId.Parse(userIdRequest.Id);

            var anUser = UserRepository.Get(userId);
            if (anUser == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            UserRepository.Remove(userId);

            var response = Request.CreateResponse(HttpStatusCode.OK);

            return response;
        }
    }
}
