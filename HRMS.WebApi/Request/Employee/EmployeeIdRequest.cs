﻿namespace HRMS.WebApi.Request.Employee
{
    using MongoDB.Bson.Serialization.Attributes;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class EmployeeIdRequest
    {
        [Description("The EmployeeId of the register user.")]
        [Required(ErrorMessage = "Missing 'EmployeeId' query parameter.")]
        [BsonId]
        public string Id { get; set; }
    }
}