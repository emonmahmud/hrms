﻿namespace HRMS.WebApi.Request.User
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using MongoDB.Bson.Serialization.Attributes;

    public class UserIdRequest
    {
        [Description("The UserId of the register user.")]
        [Required(ErrorMessage = "Missing 'UserId' query parameter.")]
        [BsonId]
        public string Id { get; set; }
    }
}
