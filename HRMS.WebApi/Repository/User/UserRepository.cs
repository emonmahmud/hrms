﻿namespace HRMS.WebApi.Repository.User
{
    using HRMS.WebApi.MongoService;
    using HRMS.WebApi.Response.User;
    using MongoDB.Bson;
    using MongoDB.Driver.Builders;
    using System;
    using System.Linq;

    public class UserRepository : IUserRepository
    {
        private readonly MongoContext<User> MongoContext;

        public UserRepository()
        {
            MongoContext = new MongoContext<User>();
        }

        public UserListResponse GetAll()
        {
            var users = MongoContext.Collection.FindAll().ToList();
            
            var userListResponse = new UserListResponse()
            {
                UserList = users.Select(user => new User()
                {
                        Id = user.Id,
                        UserName = user.UserName,
                        Password = user.Password
                }).ToList()
            };

            return userListResponse;
        }

        public User Get(ObjectId id)
        {
            var users = MongoContext.Collection.FindAll().ToList();

            var userList = users.Find(u => u.Id == id);

            return userList;
        }

        public User Add(User anUser)
        {
            if (anUser == null)
            {
                throw new ArgumentNullException("anUser");
            }

            var users = MongoContext.Collection;

            users.Insert(anUser);
            return anUser;
        }

        public bool Update(ObjectId id, User anUser)
        {
            var users = MongoContext.Collection;

            var query = Query<User>.EQ(e => e.Id, id);
            var update = Update<User>.Set(e => e.UserName, anUser.UserName).Set(e => e.Password, anUser.Password);
            users.Update(query, update);
            return true;
        }

        public void Remove(ObjectId id)
        {
            var users = MongoContext.Collection;
            var query = Query<User>.EQ(e => e.Id, id);
            users.Remove(query);
        }
    }
}
