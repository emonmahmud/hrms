﻿namespace HRMS.WebApi.Repository.User
{
    using HRMS.WebApi.MongoService;
    using HRMS.WebApi.Response.User;
    using MongoDB.Bson;

    public interface IUserRepository
    {
        UserListResponse GetAll();
        User Get(ObjectId id);
        User Add(User anUser);
        void Remove(ObjectId id);
        bool Update(ObjectId id, User anUser);
    }
}
