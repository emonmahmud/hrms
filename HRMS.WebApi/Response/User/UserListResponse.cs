﻿namespace HRMS.WebApi.Response.User
{
    using System.Collections.Generic;
    using HRMS.WebApi.MongoService;

    public class UserListResponse
    {
        public List<User> UserList { get; set; }
    }
}
