﻿namespace HRMS.WebApi.Response.Employee
{
    using System.Collections.Generic;

    public class EmployeeListResponse
    {
        public List<Models.Employee> EmployeeList {get; set;} 
    }
}
