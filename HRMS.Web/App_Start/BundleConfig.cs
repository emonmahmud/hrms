﻿namespace HRMS.Web.App_Start
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            //bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
            //          "~/Scripts/angular.min.js"));

            
            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/bootstrap.min.css",
            //          "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/hrmsangular")
                .Include("~/Scripts/hrms.routing.js")
                .Include("~/Scripts/Angular/Controller/User/hrms.userController.js")
                .Include("~/Scripts/Angular/Controller/Employee/hrms.employeeController.js")
                .Include("~/Scripts/Angular/Controller/Login/hrms.loginController.js"));


            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/Content/css/Tables.css")
                .Include("~/Content/css/bootstrap.min.css"));
        }
    }
}
