﻿var HrmsAngular = angular.module('HrmsAngularApp', [], ['$routeProvider', '$locationProvider', '$httpProvider',
    function ($routeProvider, $locationProvider, $httpProvider){
        $routeProvider
            .when('/Account/Register', { templateUrl: '/Account/Register' })
            .when('/Employee/AddEmployee', { templateUrl: '/Employee/AddEmployee' })
            .otherwise({
                redirectTo: function (x, path, y) {
                    if (path != '/')
                        window.location.href = path;
                }
            });
        $httpProvider.responseInterceptors.push('httpInterceptor');

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }
]).config(['$httpProvider', function ($httpProvider) {
    var spinnerFunction = function (data, headers) {
        $("#loading").show();

        return data;
    };
    $httpProvider.defaults.transformRequest.push(spinnerFunction);
}])
.factory('httpInterceptor', function ($q, $window, $rootScope, $location) {
    return function (promise) {

        return promise.then(function (response) {

            var headers = response.headers(),
            isNotJsonResponse = headers['content-type'] !== "application/json; charset=utf-8";

            if (isNotJsonResponse) {
                $('#loading').hide();
                return response;
            }
            else {
                if (!angular.isUndefined(response.data.redirectTo)) {
                    if (response.data.redirectTo === 'logout') {
                        window.location = '/';
                    }
                    else {
                        var redirectTo = response.data.redirectTo;

                        if ($location.path() == redirectTo) {
                            var randomNumber = Math.floor(Math.random() * 10000000);
                            $location.search("r", randomNumber);
                        }
                        else {
                            $location.path(redirectTo);
                        }

                        return false;
                    }
                }
            }
            $('#loading').hide();
            return response;
        }, function (response) {
            $('#loading').hide();
            return $q.reject(response);
        });
    };
});
