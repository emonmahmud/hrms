﻿var loginController = function ($scope, $http) {
    delete $http.defaults.headers.common['X-Requested-With'];
    
    $scope.userLogin = function () {
        var getUserApiUrl = "http://api.hrms.com/api/account/getall";
        $http.get(getUserApiUrl).success(function (response) {
            window.location.href = '/Home/Index';
        });
    };
};

loginController['$inject'] = ['$scope', '$http'];
