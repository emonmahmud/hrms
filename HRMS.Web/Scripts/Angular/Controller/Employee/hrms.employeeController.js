﻿HrmsAngular.controller('employeeController', function ($scope, $http) {
    delete $http.defaults.headers.common['X-Requested-With'];
    

    var clearStatus = function () {
        $scope.successMessage = "";
        $scope.errorMessage = "";
    };

    var mapToEmployeeModel = function (employee) {
        return {
            firstname: employee.FirstName,
            lastname: employee.LastName
        };
    };
    var resetEmployee = function(employee) {
        employee.FirstName = "";
        employee.LastName = "";
        employee.username = "";
        employee.email = "";
    };

    $scope.addNewEmployee = function(employee) {
        resetEmployee(employee);
        $scope.initialize();
    };

    $scope.createEmployee = function(employee) {
        clearStatus();
        
        $scope.submitForm = function (isValid) {
            if (isValid) {
                var postEmployeeApiUrl = 'http://api.hrms.com/api/employee/post';
                var putEmployeeApiUrl = 'http://api.hrms.com/api/employee/put/';
                
                var employeeModel = mapToEmployeeModel(employee);

                if ($scope.employee.Id == null) {
                    $http.post(postEmployeeApiUrl, employeeModel).success(function(response) {
                        $scope.employees.push(employee);
                        $scope.initialize();
                        $scope.successMessage = "Saved an employee successfully. Add another employee";
                        resetEmployee(employee);
                    });
                } else {
                    for (var i in $scope.employees) {
                        if ($scope.employees[i].Id == $scope.employee.Id) {
                            var empId = $scope.employee.Id;
                            var url = putEmployeeApiUrl + empId;
                            var empModel = mapToEmployeeModel(employee);
                            $http.put(url, empModel).success(function(response) {
                                $scope.employees[i] = $scope.employee;
                                $scope.initialize();
                                $scope.successMessage = "Updated an employee successfully.";
                                resetEmployee($scope.employee);
                            });
                        }
                    }
                }
                
            }
        };
    };

    $scope.delete = function(id) {
        var deleteEmployeeApiUrl = "http://api.hrms.com/api/employee/delete/" + id;

        $http.delete(deleteEmployeeApiUrl).success(function () {
            $scope.initialize();
            $scope.successMessage = "Deleted employee successfully.";
        }).error(function (error) {
            $scope.errorMessage = error;
        });
    };

    $scope.edit = function (id) {
        for (var i in $scope.employees) {
            if ($scope.employees[i].Id == id) {
                $scope.employee = angular.copy($scope.employees[i]);
            }
        }
    };
    
    $scope.getAllEmployees = function () {
        var getEmployeeApiUrl = "http://api.hrms.com/api/employee/getall";
        $http.get(getEmployeeApiUrl).success(function (response) {
            $scope.employees = response.EmployeeList;
        });
    };
    
    $scope.initialize = function () {
        $scope.getAllEmployees();
    };

    $scope.initialize();
});
