﻿angular.module('HrmsAngularApp').controller('userController', function ($scope, $http, userService) {
    delete $http.defaults.headers.common['X-Requested-With'];

    $scope.users = [];
    $scope.currentUser = { adding: false };

    var mapToUserModel = function (user) {
        return {
            UserName: user.UserName,
            Password: user.Password,
        };
    };

    var clearStatus = function () {
        $scope.successMessage = "";
        $scope.errorMessage = "";
    };
    
    $scope.getAllUsers = function () {
        clearStatus();
        var getUserApiUrl = "http://api.hrms.com/api/account/getall";

        userService.getUsers(getUserApiUrl).success(function (response) {
            debugger;
            $scope.users = response.UserList;
        });
    };


    $scope.createUser = function (user) {
        clearStatus();
        var postUserApiUrl = 'http://api.hrms.com/api/account/post';

        var userModel = mapToUserModel(user);
        $http.post(postUserApiUrl, userModel).success(function (response) {
            $scope.users.push(user);
            $scope.currentUser = { adding: false };
            $scope.initialize();
            $scope.successMessage = "Saved user successfully.";
        }).error(function (error) {
            $scope.errorMessage = error;
        });
    };

    $scope.updateUser = function (user) {
        clearStatus();

        var putUserApiUrl = "http://api.hrms.com/api/account/put/" + user.Id;

        var userModel = mapToUserModel(user);

        $http.put(putUserApiUrl, userModel).success(function (response) {
            user.editing = false;
            $scope.successMessage = "Updated user successfully.";
        }).error(function (error) {
            $scope.errorMessage = error;
        });
    };

    $scope.deleteUser = function (user) {
        clearStatus();

        var deleteUserApiUrl = "http://api.hrms.com/api/account/delete/" + user.Id;

        $http.delete(deleteUserApiUrl).success(function (response) {
            user.editing = false;
            $scope.initialize();
            $scope.successMessage = "Deleted user successfully.";
        }).error(function (error) {
            $scope.errorMessage = error;
        });
    };


    $scope.cancel = function () {
        clearStatus();

        $scope.currentUser = { adding: false };
        $scope.getAllUsers();
    };

    $scope.initialize = function () {
        $scope.getAllUsers();
    };

    $scope.initialize();
});
