﻿HrmsAngular.service('userService', ['$http', function ($http) {
    this.getUsers = function (apiUrl) {
        return $http.get(apiUrl);
    };
}
]);
